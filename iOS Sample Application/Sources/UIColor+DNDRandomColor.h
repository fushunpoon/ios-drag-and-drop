//
//  UIColor+DNDRandomColor.h
//  iOS Sample Application
//
//  Created by Markus Gasser on 3/3/13.
//  Copyright (c) 2013 Team RG. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor (DNDRandomColor)

+ (instancetype)randomColor;

@end
